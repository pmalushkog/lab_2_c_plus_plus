#ifndef LAB2_SEQUENCE_H
#define LAB2_SEQUENCE_H

#include <iostream>
#include <cstring>

template <class Type>
class Sequence {   // Создаем класс Sequence

private:
    Type* data;   // Объявляем указатель на перменную data
    int size = 0;   // Объвляем переменную в которой будет храниться длина
    int last = 0;   // Объявляем переменную в которой будет храниться номер последнего элемента при обработке массива

public:

    Sequence() {   // Конструктор класса Sequence  в котором инициализируются переменные из private в public
        this->size = 0;
        this->last = 0;
        this->data = new Type[0];
    }

    Sequence(int size) {   // Конструктор класса Sequence который выделяет память под массив размером size и обрабатывает ошибку на допустимую длину
        if (size < 0){
            throw std::out_of_range{"Sequence-->error-->out of range"};
        }
        this->size = size;
        this->last = 0;
        this->data = new Type[size];
    }

    Sequence(Type *items, int count) {   // Конструктор класса Sequence который производит копирование значений из заданного массива
        if (count < 0){
            throw std::out_of_range{"Sequence-->error-->out of range"};
        }
        size = count;
        data = new Type[count];

        memcpy(data, items, sizeof(Type) * count);
        size = count;
        last = count;
    }

    Sequence(const Sequence<Type> &list) {   // Конструктор который создает аналогичный list список
        this->data = new Type[list.size];   // Выделяем память
        this->size = list.size;            // Передаем размер
        this->last = list.last;           // Передаем последний элемент

        for(int i = 0; i < list.size; i++){
            this->data[i] = list.data[i];   // Записываем значения в массив-копию
        }
    }

    ~Sequence() {   // Деструктор класса Sequence
        delete[] this->data;
    }

    Type getFirst() {    // Функция которая показывает первый элемент
        return this->data[0];
    }

    Type getLast() {    // Функция которая показывает последний элемент
        return this->data[this->last-1];
    }

    Type get(int index) {   // Функция которая выводит элемент по индексу и обрабатывает ошибку на допустимое значение индекса
        if (index > last){
            throw std:: out_of_range{"index > last element"};
        }
        return this->data[index];
    }

    int getLength() {    // Функция которая выводит размер
        return this->size;
    }

    int getFullLength() {   // Функция которая выводит размер
        return this->size;
    }

    void append(Type item) {   // Функция которая добавляет элемент с конца
        this->data[this->last++] = item;
    }

    void prepend(Type item) {   // Функция которая добавляет элемент в начало и обрабатывает ошибку
        if (this->last == this->size){
            throw std::out_of_range{"Max_size:Error:prepend"};
        }
        this->last++;
        for(int i = this->last - 1; i >= 1; i--){
            this->data[i] = this->data[i - 1];
        }
        this->data[0] = item;
    }

    void insert(Type item, int index) {   // Функция которая добавляет элемент в указанное место
        if ((index > this->last) or (index > this->size) or (index < 0)){
            throw std::out_of_range{"Sequence-->error-->out of range"};
        }
        if (index <= this->last){
            for (int i = this->last-1; i >= index; i--) {
                this->data[i+1] = this->data[i];
            }
            this->data[index] = item;
            this->last++;
        } else {
            this->data[this->last++] = item;
        }
    }

    Sequence<Type> concat(Sequence<Type> &seq) {   // Функция объединения 2-х массивов

        Sequence<Type> res(size + seq.size);
        for(int i = 0; i < size; i++){
            Type value = get(i);
            res.append(value);
        }
        for(int i = 0; i < seq.getLength(); i++){
            Type value = seq.get(i);
            res.append(value);
        }

        return res;
    }

    Sequence<Type> getSubsequence(int start, int end) {    // Функция которая выделяет подпоследовательность с i по k элементы
        if (start < 0 or end < 0 or start > this->last-1 or end > this->last-1 or start > this->size or end > this->size ){
            throw std::out_of_range{"Sequence-->error-->wrong index"};
        }
        Sequence<Type> subList(end - start + 1);
        for (int i = start; i <= end; i++){
            Type value = get(i);
            subList.append(value);
        }
        return subList;
    }

    Sequence<Type> &operator=( const Sequence<Type> &arr) {   // Переопределяем оператор присваивания
        this->~Sequence();
        if (arr.size > 0){
            data = new Type[arr.size];
            memcpy(data, arr.data, sizeof(Type) * arr.size);
        } else {
            data = new Type[0];
        }
        size = arr.size;
        last = arr.last;
        return *this;
    }

    void print() {   // Функция печати элемента
        for(unsigned i = 0; i < this->last; i++){
            std::cout << this->data[i] << " ";
        }
    }

    bool operator==(const Sequence<Type> &seq) {   // Переопределяем оператор равнетсва
        if (seq.size != this->size){
            return false;
        }
        for(int i = 0; i < this->size; i++){
            if (this->get(i) != seq.get(i)){
                return false;
            }
        }
        return true;

    }
};

#endif //LAB2_SEQUENCE_H
