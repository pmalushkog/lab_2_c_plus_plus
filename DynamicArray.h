#ifndef LAB2_DYNAMICARRAY_H
#define LAB2_DYNAMICARRAY_H

#include <iostream>
#include <cstring>

template <class Type> // Создаем шаблон класса
class DynamicArray
{
private:
    Type* data; // Создаем переменную-указатель на первый элемент заданного типа
    int size = 0; // Размер массива по умолчанию 0

public:
    DynamicArray() {  // Коструктор класса DA в котором мы устанавливаем значения data и size(т.к они изначально находятся в private)
        data = new Type[1];
        size = 0;
    }

    DynamicArray(int size) {  //конструктор класса DA в котором происходит обработка ошибки на случай размера < 0, значению size присваивается размер массива,
        if (size < 0){        // а для значения data выделаяется память под нужный нам тип
            throw std::out_of_range{"size < 0"};
        }

        this->size = size;
        this->data = new Type[size];
    }

    DynamicArray(Type *items, int count) { // конструктор класса DA где происходит копирование элементов из переданного массива, а так же обработка ошибки на случай
        if (count < 0){                                                                                //отрицательного значения count(размера передаваемого массива)
            throw std::out_of_range{"count < 0"};
        }
        this->size = count;
        this->data = new Type[count];

        for(size_t i = 0; i < count; i++){
            this->data[i] = items[i];
        }
    }

    DynamicArray(const DynamicArray<Type> &arr) { // Копирующий конструктор класса DA. Создает аналогичный список. Передается тип данных и ссылка на 1-й элемент arr
        this->data = new Type[arr.size]; // так как size задан конструктором выделяется память под массив
        this->size = arr.size; // В параметр size передается значение size для arr

        for(int i = 0; i < arr.size; i++){
            this->data[i] = arr.data[i]; // Передается элемент из основного массива в массив arr
        }
    }

    Type &operator[](int i) { // Переопределение оператора обращения по индексу
        return this->data[i];
    }

    Type get(int i) {   // Фнукция в классе DA которая позволяет получить элемент массива по индексу
        return this->data[i];
    }

    int getSize() {  // Функция в классе DA которая позволяет получить размер массива
        return this->size;
    }

    void set(int index, Type value) {   // Функция в классе DA которая позволяет задать элемент по индексу принимая номер элемента и его тип и значение,
                                                                                                                         // а так же обрабатывает ошибки
        if ((index < 0) or (this->size < index)){
            throw std::out_of_range{"index < 0 or this->size < index"}; // обработка ошибки на случай выхода за рамки размера массива
        }
        data[index] = value; // присваивание значения элементу
    }

    void resize(int newSize) {  // Функция в классе DA которая позволяет задать новый размер массива принимая новый размер. Если новый размер больше то записывает
                                                    // элементы в начало если меньше то отбрасывает элементы которые не поместились, а так же обрабатывает ошибки
        if (newSize < 0 ){ // Проверка значения newSize на допустимое
            throw std::out_of_range{"newSize < 0"};
        }
        Type *newData = new Type[newSize]; // Выделяется память для нового массива с новым размером

        if (newSize > 0) {
            std::memcpy(newData, data, sizeof(Type) * newSize); // Копирование элементов memcpy(указатель на то куда, указатель на то откуда,
                                                                                                                                    // количество копируемых байтов)
        } else {
            newData = new Type[0]; // Если размер нового массива нулевой, то создает пустой массив
        }
        data = newData; // Записываются обновленные параметры
        size = newSize;
    }

    void insert(int index, Type value) { // Функция в классе DA которая присваивает номеру элемента значение принимая номер элемента и значение с его типом.
                                                                                                 // Используется в ArraySequence, а так же обрабатывает ошибки
        if (index > size){ // Обработка ошибки
            throw std::out_of_range{"index > size"};
        }
        data[index] = value;

    }

};

#endif //LAB2_DYNAMICARRAY_H
