#ifndef LAB2_MENU_H
#define LAB2_MENU_H

#include <iostream>
#include <complex>
#include <Windows.h>
#include "Vector.h"
#include "Sequence.h"

using namespace std;

template <typename Type>
int menu(){
    SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);
        int size;
        int oper;
        cout << "Возможные действия:\n";
        cout << "1. Сумма\n2. Умножение на скаляр\n3. Скалярное произведение векторов\n4. Вычисление нормали\n\n5. Выход из программы\n" << endl;
        cin >> oper;
        if (oper == 5){
            return 0;
        }
        cout << "Какой размерности будут вектора?\n";
        cin >> size;
        Vector<Type> a(size);

        if (oper == 1){
            Vector<Type> b(size);
            Vector<Type> c(size);
            cout << "Введите координаты первого вектора:\n";
            inputVec(a);
            cout << "Введите координаты второго вектора:\n";
            inputVec(b);
            c = a + b;
            cout << "Результат сложения векторов:" << endl;
            print(c);

        } else if(oper == 2){
            Vector<Type> c(size);
            Type scalar;
            cout << "Введите координаты первого вектора:\n";
            inputVec(a);
            cout << "Введите число на которое будет умножен вектор:\n";
            cin >> scalar;
            c = a * scalar;
            cout << "Результат умножения на число:\n";
            print(c);

        }  else if(oper == 3){
            Vector<Type> b(size);
            Type scalar;
            cout << "Введите координаты первого вектора::\n";
            inputVec(a);
            cout << "Введите координаты второго вектора:\n";
            inputVec(b);
            scalar = a.dot(b);
            cout << "Результат скалярного умножения:" << endl;
            cout << scalar << endl;

        }else if(oper == 4){
            Type scalar;

            cout << "Введите координаты первого вектора::\n";
            inputVec(a);
            scalar = a.norm();
            cout << "Нормаль данного вектора:\n";
            cout << scalar << endl;
        } else {
            cout << "Ошибка выбора операции" << endl;
            oper = -1;
        }
    }

#endif //LAB2_MENU_H
