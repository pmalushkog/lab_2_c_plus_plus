#ifndef LAB2_LINKEDLIST_H
#define LAB2_LINKEDLIST_H

template<class T> // Создаем шаблон класса LL
class LinkedList
{
private:
    template<typename Type> // Создаем шаблон класса Transition в модификатеоре доступа private класса LL
    class Transition{
    public:
        Type data; // Задаем переменную типа Type в которой будет храниться сам элемент
        Transition<Type> *next;  // Задаем перменную-указатель класса Transition которая будет указывать на следующий элемент связного списка

        Transition(Type data, Transition *next = nullptr){  // Конструктор класса Transition где происходит инициализация в которую передается сам
                                                                                                          // элемент и нулевой указатель наследующий элемент
            this->data = data;  // Задается элемент
            this->next = next;  // Задается указатель
        }
    };

    int size = 0;   // Инициализируется размер списка
    Transition<T> *head; // Объявляется указатель на переменную head в классе Transition но типа T(тип класса LL)

public:

    LinkedList() {  // Коструктор в классе LL который создает пустой список
        this->size = 0;
        this->head = nullptr;
    }

    LinkedList(T* items, int count) // Коструктор в классе LL который копирует элементы из заданного массива
                                         // в список в который передается указатель типа T и количство элементов
    {
        for( int i = 0; i < count; i++){
            append(items[i]);  // Добавлятеся элемент в список
        }
        this->size = count;  // Записывается размер массива
    }
    LinkedList( LinkedList<T> &list) // Копирующий конструктор в классе LL в который передается класс LL list
    {
        Transition<Type>* tmp = list.head;
        for(int i = 0; i < list.size; i++){
            append(tmp->data);  // Копируем элемент в data
            tmp = tmp->next;  // Переходим к следующему
        }

    }

    ~LinkedList() {  // Деструктор в классе LL
        while (this->size){

            Transition<T> *temp = head;
            head = head->next;
            delete temp;
            size--;
        }
    }

    T getFirst() { return this->head->data; }   // Функция получения первого элемента в списке

    T getLast() { // Функция получения последнего элемента в списке
        int counter = 1;
        Transition<T> *current = this->head;

        while( current != nullptr){  // Пока не вышли за границы списка
            if (counter == this->size){
                return current->data;
            }
            current = current->next;
            counter++;
        }
    }

    T get(int index) {   // Функция получения элемента по индексу
        int counter = 0;
        Transition<T> *current = this->head;

        while( current != nullptr){
            if (counter == index ){
                return current->data;
            }
            current = current->next;
            counter++;
        }
    }

    int getLength() { return this->size; }  // Функция получения длины списка

    void append(T item)   // Функция добавления элемента в конец списка
    {
        if (head == nullptr){
            head = new Transition<T>(item);
        } else {

            Transition<T> *current = this->head;
            while(current->next != nullptr){   //Доходим до последнего элемента
                current = current->next;
            }
            current->next = new Transition<T>(item); // Инициализируется значение по адресу current->next равное item

        }
        this->size++;   // Соостветсвенно увеличиваем размер массива
    }

    void prepend(T item) {  // Функция добавления элемента в начало списка

        this->head = new Transition<T>(item, this->head);
        this->size++;
    }

    void insert(T item, int index) {   // Функция вставки элемента в заданную позицию

        if (index == 0){
            prepend(item);

        } else {
            Transition<T> *previous = this->head;

            for(int i = 0; i < index -1 ;i++ ){
                previous = previous->next;
            }

            auto *newTransition = new Transition<T>(item, previous->next);
            previous->next = newTransition;
            this->size++;
        }
    }
    void concat(LinkedList<T> &list) {   // Функция объединенеия двух списков где на вход да
        for (int i = 0; i < list.getLength(); i++){
            append(list.get(i));   // Вставка элемента с использованием нашей функции get получения элемента по индексу
        }

    }

    LinkedList<T> getSubList(int start, int end){   // Получение списка из i по k элементов
        LinkedList<T> subList;
        for (int i = start; i <= end; i++){
            subList.append(get(i));
        }
        return subList;
    }

};

#endif //LAB2_LINKEDLIST_H
