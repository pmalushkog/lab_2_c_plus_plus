#ifndef LAB2_ARRAYSEQUENCE_H
#define LAB2_ARRAYSEQUENCE_H

#include "Sequence.h"
#include "DynamicArray.h"


template <class T>
class ArraySequence: public Sequence<T> {

private:
    DynamicArray<T> items;

public:
    ArraySequence() {
        items = DynamicArray<T>();
    }

    ArraySequence(T *item, int count) {
        items = DynamicArray<T>(item, count);
    }

    ArraySequence(DynamicArray<T> *arr) {
        items = arr;
    }

    int getLength() {
        return items.getSize();
    }

    T getFirst() {
        return items.get(0);
    }

    T getLast() {
        return items.get(items.getSize() - 1);
    }

    T get(int index) {
        return items.get(index);
    }

    void append(T item) {
        items.resize(items.getSize() + 1);
        items.insert(items.getSize() - 1, item);
    }

    void prepend(T item) {
        items.resize(items.getSize() + 1);
        for (int i = items.getSize() - 1; i >= 1; i--) {
            items.set(i, items.get(i - 1));
        }
        items.set(0, item);
    }

    void insert(T item, int index) {
        items.insert(index, item);
    }

    ArraySequence<T> getSubSequence(int start, int end) {
        ArraySequence<T> arr{};

        for (int i = start; i <= end; i++){
            arr.append(items.get(i));
        }

        return arr;
    }

    ArraySequence<T> concat(ArraySequence<T> &seq) {
        ArraySequence<T> res{};
        for (int i = 0; i < items.getSize(); i++) {
            res.append(items[i]);
        }
        for (int i = 0; i < seq.getLength(); i++) {
            res.append(seq.get(i));
        }
        return res;
    }
};


#endif //LAB2_ARRAYSEQUENCE_H
