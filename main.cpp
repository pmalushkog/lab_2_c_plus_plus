#include <iostream>
#include <complex>
#include <Windows.h>
#include "menu.h"
using namespace std;

int main() {

    SetConsoleCP(CP_UTF8);
    SetConsoleOutputCP(CP_UTF8);

    while(true) {

        int type;

        cout << "\nВыберите тип координат векторов:\n";
        cout << "1. Целые числа\n2. Нецелые числа\n3. Комплексные числа\n\n4. Выход из программы\n" << endl;
        cin >> type;

        if (type == 1){
            menu<int>();
        } else if(type == 2){
            menu<float>();
        } else if(type == 3) {
            menu<complex<float>>();
        } else if (type == 4) {
            cout <<"Выход из программы..."<< endl;
            break;
        } else {
            cout << "Ошибка выбора типа, попробуйте снова:" << endl;
            type = -1;
        }
    }

    return 0;
}
