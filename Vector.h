#ifndef LAB2_VECTOR_H
#define LAB2_VECTOR_H

#include "Sequence.h"
#include <iostream>
#include <complex>


template <class T>
class Vector {
private:
    Sequence<T> items;   // Объявляем переменную типа T класса Sequence

public:
    Vector() {
        items = Sequence<T>();   // Конствуктор в котором инициализируется переменная items в public
    }

    Vector(int size) {
        items = Sequence<T>(size);   // Конструктор в котором инициализируется размер массива в классе Sequence
    }

    Vector(T *item, int count) {   // Коструктор в котором копируются значения элементов заданного массива
        items = Sequence<T>(item, count);
    }

    void push_back(T item) {   // Функция в которой добавляется 1 элемент массива
        items.append(item);
    }

    T operator[](int i) {   // Функция в которой выводится элемент по индексу
       return items.get(i);
    }

    int getSize() {   // Фукнция которая возвращает длину списка
        return items.getLength();
    }

    int getFullSize() {  // Функция которая возвражается длину списка
        return items.getFullLength();
    }

    Vector<T> &operator=(const Vector<T> &v) {
        items = v.items;
        return *this;
    }

    Vector<T> &operator+(const Vector<T> &v) {
        auto seq = v.items;
        auto res = Sequence<T>(seq.getLength());

        for (int i = 0; i < items.getLength(); i++){
            T value = items.get(i) + seq.get(i);
            res.append(value);
        }
        items = res;
        return *this;
    }

    Vector<T> &operator-(const Vector<T> &v) {
        auto seq = v.items;
        auto res = Sequence<T>(seq.getLength());

        for (int i = 0; i < items.getLength(); i++){
            T value = items.get(i) - seq.get(i);
            res.append(value);
        }
        items = res;
        return *this;
    }

    Vector<T> &operator*(T item) {
        auto nv = Vector<T>(items.getLength());
        for (int i = 0; i < items.getLength(); i++){
            nv.push_back( item * items.get(i));
        }
        items = nv.items;
        return *this;
    }

    bool operator==(const Vector<T> &v) {   // Оператор который сравнивает два элемента на эквивалентность возвращая bool
        return this->items == v.items;
    }

    T dot(Vector<T> v) {   //   Функция скалярного произвдения нашего вектора и поступающего на вход и обрабатывает ошибку на случай разных длин векторов
        if (v.getSize() != items.getLength()){
            throw std::out_of_range{"different Vector sizes"};
        }
        T sum = items.get(0) * v[0];
        for (int i = 1; i < items.getLength(); i++){
            sum += items.get(i) * v[i];
        }
        return sum;
    }

    T norm() {   // Функция
        //T sum = items.get(0) * items.get(0);
        T sum = 0;
        for (int i = 0; i < items.getLength(); i++){
            sum += items.get(i) * items.get(i);
        }
        return sqrt(sum);
    }

    ~Vector() = default;
};

template <typename Type>   // Шаблон функции вне классе которая задает массив
void inputVec(Vector<Type> &a){
    for(int i = 0; i < a.getFullSize(); i++){
        Type x;
        std::cin >> x;
        a.push_back(x);
    }
}

template <typename Type>   // Шаблон функции вне класса которая печатает массив
void print(Vector<Type> v){
    for(int i = 0; i < v.getFullSize(); i++){
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

#endif //LAB2_VECTOR_H
