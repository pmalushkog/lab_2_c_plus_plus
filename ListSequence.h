#ifndef LAB2_LISTSEQUENCE_H
#define LAB2_LISTSEQUENCE_H

#include "Sequence.h"
#include "LinkedList.h"


template <class T>
class ListSequence: public Sequence<T> {

private:
    LinkedList<T> items;

public:
    ListSequence() {
        items = LinkedList<T>();
    }

    ListSequence(T *item, int count){
        items = LinkedList<T>(item, count);
    }

    ListSequence(LinkedList<T> *arr) {
        items = arr;
    }

    int getLength() {
        return items.getLength();
    }

    T getFirst() {
        return items.getFirst();
    }

    T getLast() {
        return items.getLast();
    }

    T get(int index) {
        return items.get(index);
    }

    void append(T item) {
        items.append(item);
    }

    void prepend(T item) {
        items.prepend(item);
    }

    void insert(T item, int index) {
        items.insert(item, index);
    }

    ListSequence<T> *getSubSequence(int start, int end) {
        auto *res = new ListSequence<T>();
        res->items = items.getSubList(start, end);
        return res;
    }

    ListSequence<T> concat(ListSequence<T> &list) {
        ListSequence<T> res{};
        for (size_t i = 0; i < items.getLength(); ++i) {
            res.append(items.get(i));
        }
        for (size_t i = 0; i < list.getLength(); ++i) {
            res.append(list.get(i));
        }
        return res;
    }
};

#endif //LAB2_LISTSEQUENCE_H
